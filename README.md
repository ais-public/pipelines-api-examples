# Pipelines API Examples

This repository contains examples of how to use the AIS Pipelines REST API, which is hosted at `https://api.pipelines.ati.ausport.gov.au`.

### Setup

You'll require a Pipelines API key to use the API. If you don't yet have an API key,
you can get one by opening a pipeline in the [Pipelines web app](https://pipelines.ati.ausport.gov.au/)
and hitting the little "profile" icon to the top right of the screen. Then click "API Access"
to generate and copy your key.

If this menu option is missing, email `AISDevelopers@ausport.gov.au` to request access.

### Running the examples

#### Python

At this point, Python is the only language for which we have examples. these are located in the `./python` directory.

It's recommended to look through `./python/api_utils.py` for the actual endpoints being called, and using the Jupyter notebook `./python/pipelines_workflow.ipynb` for a practical use case.

You'll need to install the requirements, as found in `./python/requirements.txt`. You can do this by running:

```bash
pip install -r ./python/requirements.txt
```

It's recommended that you use a virtual environment using `venv`, `conda` or similar.

Then, you can either run the Jupyter notebook by running:

```bash
jupyter notebook ./python/pipelines_workflow.ipynb
```

Or you can run the Streamlit example app by running:

```bash
streamlit run ./python/Streamlit_Demo.py
```

### Links:

- [Pipelines endpoints (documentation)](https://api.pipelines.ati.ausport.gov.au/api-docs/)
- [Pipelines web app](https://pipelines.ati.ausport.gov.au/)
