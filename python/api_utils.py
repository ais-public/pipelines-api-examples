"""
Utilities for interacting with the Pipelines API.
"""

import requests

API_URL = 'https://api.pipelines.ati.ausport.gov.au'

REQ_TIMEOUT = 10
EXTENDED_TIMEOUT = 120

s = requests.Session()
s.params = {'timeout': REQ_TIMEOUT}


def set_api_key(api_key: str):
    """
    Set the API key to be used in the requests.
    """
    s.headers['X-SAM-API-Key'] = api_key


def assert_key_present():
    """
    Assert that the API key is set.
    """
    if 'X-SAM-API-Key' not in s.headers:
        raise ValueError('API key not set')


def generate_blob_upload_url_and_id(filename: str, pipeline_id: str, node_id: str,
                                    property_id: str, blob_type: str):
    """
    Generate an upload URL and blob ID for the given file and pipeline.
    """
    assert_key_present()

    payload = {
        'blobName': filename,
        'originalFilename': filename,
        'graph': pipeline_id,
        'nodeId': node_id,
        'propertyId': property_id,
    }
    response = s.post(f'{API_URL}/blobs/{blob_type}', json=payload)
    response.raise_for_status()

    res_json = response.json()
    return res_json['uploadUrl']['url'], res_json['id']


def upload_file(filedata: bytes, upload_url: str, blob_id: str):
    """
    Upload the given file data to the given upload URL and mark the upload as finished
    on its corresponding pipeline.
    """
    assert_key_present()

    response = requests.post(upload_url, headers={
        'X-Goog-Resumable': 'start',
        'Content-Type': 'application/octet-stream',
    }, timeout=REQ_TIMEOUT)
    response.raise_for_status()

    upload_location = response.headers.get('Location')
    if not upload_location:
        raise ValueError('Failed to get upload location')

    requests.put(upload_location, data=filedata,
                 timeout=EXTENDED_TIMEOUT).raise_for_status()

    # Mark the upload as finished
    s.patch(f'{API_URL}/blobs/{blob_id}/upload-finished').raise_for_status()


def start_job(pipeline_id: str, config_override: dict = None):
    """
    Start a job for the given pipeline, with an optional node config override dict.
    """
    assert_key_present()

    body = dict(configOverride=config_override or {})

    s.post(f'{API_URL}/graphs/{pipeline_id}/job',
           json=body).raise_for_status()


def cancel_job(pipeline_id: str):
    """
    Cancel the job for the given pipeline.
    """
    assert_key_present()

    response = s.delete(f'{API_URL}/graphs/{pipeline_id}/job')
    response.raise_for_status()


def get_job_status_and_percentage(pipeline_id: str):
    """
    Get the status and completion percentage of the job for the given pipeline.
    """
    assert_key_present()

    response = s.get(f'{API_URL}/graphs/{pipeline_id}/job', )
    response.raise_for_status()

    job_state = response.json()
    job_status = job_state['status']
    completion_percentage = job_state['completionPercent']

    return job_status, completion_percentage


def get_blobs(pipeline_id: str):
    """
    Get the blobs for the given pipeline.
    """
    assert_key_present()

    response = s.get(f'{API_URL}/graphs/{pipeline_id}/blobs')
    response.raise_for_status()
    return response.json()


def get_pipelines():
    """
    Get the user's pipelines.
    """
    assert_key_present()

    response = s.get(f'{API_URL}/graphs')
    response.raise_for_status()
    return response.json()


def get_blob_for_pipeline(pipeline_id: str, node_id: str, property_id: str):
    """
    Get the blob for the given pipeline, node and property.
    """
    assert_key_present()

    blobs = get_blobs(pipeline_id)
    blob = next((blob for blob in blobs
                 if blob['nodeId'] == node_id and blob['propertyId'] == property_id
                 ), None)

    return blob


def download_file(download_url: str, filename: str):
    """
    Download a file from the given URL and save it to the given filename.
    """
    response = requests.get(download_url, timeout=EXTENDED_TIMEOUT)
    response.raise_for_status()

    with open(filename, 'wb') as f:
        f.write(response.content)
    return filename
