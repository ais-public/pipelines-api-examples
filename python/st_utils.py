"""
A collection of utility functions for Streamlit components.
"""

import time
from tempfile import NamedTemporaryFile

import api_utils
import streamlit as st

# All the keypoints that are tracked by our pose estimation model.
KEYPOINTS = (
    'right_ankle', 'right_knee', 'right_hip', 'right_wrist', 'right_elbow', 'right_shoulder',
    'left_ankle', 'left_knee', 'left_hip', 'left_wrist', 'left_elbow', 'left_shoulder',
    'head_top', 'head', 'neck', 'spine', 'pelvis', 'right_toe_base', 'right_heel', 'left_toe_base',
    'left_heel'
)


def api_key_input():
    """
    Streamlit component to input the Pipelines API key  in the sidebar, set it in the requests
    object and validate it. Returns the API key or None if invalid or empty.
    """
    api_key = persisted_text_input(key='api_key',
                                   label='Pipelines API key',
                                   help='Enter your Pipelines API key here.',
                                   container=st.sidebar)
    if not api_key:
        st.sidebar.error(
            '☝️ Enter your Pipelines API key to get started')
        return None

    api_utils.set_api_key(api_key)
    try:
        # Just fetch the pipelines to validate the API key.
        api_utils.get_pipelines()
    except Exception:
        st.sidebar.error(
            'Invalid API key, please try again.')
        return None

    return api_key


def pipeline_picker():
    """
    Streamlit component to select a pipeline from a dropdown.
    """
    pipelines = api_utils.get_pipelines()

    pipeline_names = [p['name'] for p in pipelines]
    selected_pipeline = st.selectbox(
        'Select a pipeline', pipeline_names, index=None, placeholder='Select a pipeline')

    if selected_pipeline:
        return pipelines[pipeline_names.index(selected_pipeline)]
    return None


def persisted_text_input(key, label, default=None, container=None, **kwargs):
    """
    Streamlit component to create a text input field state that persists
    throughout the session.
    """
    container = container or st
    value = container.text_input(
        label, value=st.session_state.get(key, default), **kwargs)
    st.session_state[key] = value
    return value


def blob_uploader(filedata: bytes, filename: str, pipeline_id: str,
                  input_node_id: str, input_property_id: str, blob_type: str):
    """
    Streamlit component to upload a file to the server and display its progress.
    """
    status = st.empty()
    status.info('Getting upload URL...')
    upload_url, file_id = api_utils.generate_blob_upload_url_and_id(
        filename, pipeline_id, input_node_id, input_property_id, blob_type)
    status.info(f'Uploading {filename}...')
    api_utils.upload_file(filedata, upload_url, file_id)
    status.success(f'{filename} uploaded!')


def job_progress_follower(pipeline_id: str) -> str:
    """
    Streamlit component to track and display the progress of a job.
    """
    prog_bar = st.empty()
    cancel_btn = st.empty()

    cancel_btn.button(
        'Cancel job', on_click=lambda: api_utils.cancel_job(pipeline_id))
    job_status = 'QUEUED'
    while job_status in ('QUEUED', 'STARTING', 'RUNNING'):
        job_status, completion_percentage = api_utils.get_job_status_and_percentage(
            pipeline_id)
        prog_bar.progress(float(completion_percentage or 0),
                          text=f'Job {job_status.lower()}')
        time.sleep(1)

    cancel_btn.empty()
    prog_bar.empty()

    return job_status


def output_blob_downloader(pipeline_id: str, blob_data_type: str):
    """
    Streamlit component to download an output blob from the server,
    based on the blob type. Returns the first blob of the given type.
    """
    blobs = api_utils.get_blobs(pipeline_id)

    if not blobs:
        st.error('No blobs found!')
        return None, None

    blobs = [
        b for b in blobs
        if b['blobType'] == 'OUTPUT' and
        b['blobDataType'] == blob_data_type]

    if not blobs:
        st.error(f'No {blob_data_type} blobs found!')
        return None, None

    blob = blobs[0]
    status = st.empty()
    blob_name = blob['blobName']
    download_url = blob['downloadUrl']['url']
    status.info(f'Fetching {blob_name}...')

    filename = NamedTemporaryFile(suffix=blob_name).name
    filename = api_utils.download_file(download_url, filename)

    status.success(f'{blob_name} fetched!')
    return filename, download_url


def joint_selector():
    """
    Streamlit component to select a joint from a dropdown.
    """
    return st.selectbox('Joint', KEYPOINTS, index=0)
