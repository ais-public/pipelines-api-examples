"""
Plots the motion of a joint over time for multiple athletes in a video.

Requirements:
- requests
- streamlit
- pandas
- seaborn
- matplotlib
"""

import asyncio

import api_utils as api_utils
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import st_utils

import streamlit as st

DEFAULT_INPUT_NODE_ID = 'VideoLoader'
INPUT_KEY = 'source'
VIDEO_OUTPUT_KEY = 'video'


async def on_start_job(videodata,
                       data_filename,
                       pipeline_id,
                       input_node):
    # Upload the video file
    st_utils.blob_uploader(
        videodata, data_filename, pipeline_id, input_node, INPUT_KEY, 'video')

    # Start the job and wait for it to finish
    api_utils.start_job(pipeline_id)
    job_status = st_utils.job_progress_follower(pipeline_id)

    if 'fail' in job_status.lower():
        st.error('Job failed!')
        return
    st.success('Job completed successfully!')

    # Download the output files
    output_video_filename, _ = st_utils.output_blob_downloader(
        pipeline_id, 'VIDEO'
    )
    if output_video_filename is None:
        return

    output_csv_filename, output_csv_download_url = st_utils.output_blob_downloader(
        pipeline_id, 'CSV'
    )
    if output_csv_filename is None or output_csv_download_url is None:
        return

    # Place the plot in a fragment so we don't have to re-run the whole script
    # when we change the joint or track IDs.
    @st.experimental_fragment
    def plot_fragment():
        # Read the CSV into a DataFrame each time the fragment is run
        # as we'll be editing the data in-place
        df = pd.read_csv(output_csv_filename)

        st.subheader('Results')
        st.video(output_video_filename)

        st.markdown('#### Raw Pose Data')
        st.code(df)
        st.markdown(f'[Download Raw Pose Data]({output_csv_download_url})')

        st.markdown('#### Joint Motion Visualisation')

        # Find all the track IDs in the columns
        track_ids = set()
        for col in df.columns:
            if col.startswith('track_'):
                track_ids.add(int(col.split('_')[1]))
        track_ids = sorted(track_ids)

        st.markdown(
            'Select the track IDs to compare, referring to the video above.')
        chosen_ids = st.multiselect(
            'Track IDs', list(track_ids), list(track_ids))

        st.markdown('Select a joint to visualise its motion.')
        joint = st_utils.joint_selector()

        # Find all the columns (for all tracks) that match the joint
        joint_cols = [col for col in df.columns if f'_{joint}.' in col and any(
            col.startswith(f'track_{track_id}') for track_id in chosen_ids)]
        joint_xcols = [col for col in joint_cols if col.endswith('.x')]
        joint_ycols = [col for col in joint_cols if col.endswith('.y')]

        # Offset the joint values by their mean
        for joint_xcol, joint_ycol in zip(joint_xcols, joint_ycols):
            df[joint_xcol] -= df[joint_xcol].mean()
            df[joint_ycol] -= df[joint_ycol].mean()

        # Plot the x/y values of each joint against each other
        fig, ax = plt.subplots()
        ax.set_title(f'{joint}.x')
        for joint_xcol in joint_xcols:
            sns.lineplot(data=df, x='timestamp (sec)', y=joint_xcol,
                         ax=ax, sort=False)
        st.pyplot(fig)

        fig, ax = plt.subplots()
        ax.set_title(f'{joint}.y')
        for joint_ycol in joint_ycols:
            sns.lineplot(data=df, x='timestamp (sec)', y=joint_ycol,
                         ax=ax, sort=False)
        st.pyplot(fig)

    plot_fragment()


def layout_ui():
    st.title('Joint Motion Comparison')

    st.write("""
This demonstrates how to predict the pose of multiple athletes using Pipelines,
then do some do some basic plotting to compare the motion of two joints over time.
This is a simple example to show how Pipelines can be used to extract pose data
for further analysis - in this case, doing a qualitative comparison of the motion
of two athletes. This could be used for sports where athlete synchronisation is
important, such as rowing.
           
First, build a Pipeline that - at a minimum - contains the following:

1. Video Loader
2. Athlete Detector
3. Athlete Tracker
4. Pose detector
5. Tracked Pose Writer
6. Bounding Box Overlayer
7. Video Writer

Then, select that Pipeline below, verify that the Video Loader name is correct,
upload a video file and start the job. The results will be displayed at the
bottom once the job is complete.
""")

    api_key = st_utils.api_key_input()
    if not api_key:
        st.error('Please enter a valid Pipelines API key in the sidebar')
        return

    picked_pipeline = st_utils.pipeline_picker()

    if picked_pipeline is None:
        return None

    pipeline_id = picked_pipeline['id']

    input_node_id = st_utils.persisted_text_input(key='input_node_id',
                                                  label='Video input Node',
                                                  default=DEFAULT_INPUT_NODE_ID,
                                                  help='The name of the video input node.'
                                                  )

    if not input_node_id:
        st.error('Please fill in all the required fields.')
        return

    uploaded_file = st.file_uploader(
        'Select a video file', type=['mp4', 'mkv', 'avi'])

    if not uploaded_file:
        return

    st.markdown('#### Video preview')
    st.video(uploaded_file)

    start_button = st.empty()
    triggered = start_button.button('Start Pipelines job', type='primary')

    if not triggered:
        return

    start_button.empty()

    asyncio.run(on_start_job(uploaded_file.getvalue(),
                             uploaded_file.name,
                             pipeline_id,
                             input_node_id))


if __name__ == '__main__':
    layout_ui()
