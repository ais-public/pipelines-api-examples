"""
Create a Voronoi diagram from the world positions of players in a team sports setting.

Requirements:
- requests
- streamlit
- pandas
- matplotlib
- scipy
"""

import asyncio

import api_utils as api_utils
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import st_utils
import streamlit.components.v1 as components
from matplotlib.animation import FuncAnimation
from scipy.spatial import Voronoi, voronoi_plot_2d

import streamlit as st

# Increase the embed limit to allow larger animations
matplotlib.rcParams['animation.embed_limit'] = 256  # MB

DEFAULT_INPUT_NODE_ID = 'VideoLoader'
INPUT_KEY = 'source'
DATA_OUTPUT_KEY = 'output_data'


async def on_start_job(videodata,
                       video_filename,
                       pipeline_id,
                       input_node):
    # Upload the video file
    st_utils.blob_uploader(
        videodata, video_filename, pipeline_id, input_node, INPUT_KEY, 'video')

    # Start the job and wait for it to finish
    api_utils.start_job(pipeline_id)
    job_status = st_utils.job_progress_follower(pipeline_id)

    if 'fail' in job_status.lower():
        st.error('Job failed!')
        return
    st.success('Job completed successfully!')

    # Download the output file
    output_filename, output_download_url = st_utils.output_blob_downloader(
        pipeline_id, 'CSV'
    )
    if output_filename is None or output_download_url is None:
        return

    df = pd.read_csv(output_filename)

    # Retain only "world_position" columns
    cols = [col for col in df.columns if 'world_position' in col]
    df = df[cols]

    # Place the plot in a fragment so we don't have to re-run the whole script
    @st.experimental_fragment
    def plot_fragment():
        st.subheader('Results')

        st.markdown('#### Raw Data')
        st.markdown(f'[Download]({output_download_url})')
        st.dataframe(df)

        st.markdown('#### Voronoi Visualisation')

        fig, ax = plt.subplots()

        def animate(i: int):
            # Render the Voronoi diagram for the given frame number
            ax.cla()
            positions = df.iloc[i].values.reshape(-1, 2)

            positions = positions[~pd.isnull(positions).any(axis=1)]
            if len(positions) >= 3:
                vor = Voronoi(positions)
                voronoi_plot_2d(vor, ax=ax, show_vertices=False,
                                line_colors='#00cc00', line_alpha=0.5, point_size=2)
            # Add the frame number
            ax.text(0.5, 1.05, f'Frame {i}', transform=ax.transAxes,
                    ha='center', va='center')
            ax.invert_yaxis()
            return []

        # Build the animation and display it as an HTML component
        with st.spinner("Preparing animation..."):
            ani = FuncAnimation(fig, animate, frames=len(
                df), interval=30, blit=True, repeat=False)

            components.html(ani.to_jshtml(), height=1000)

    plot_fragment()


def layout_ui():
    st.title('Voronoi Diagram')

    st.write("""
This demonstrates how to derive a Voronoi diagram from the prediction location
of players in a team sports setting. Note that there is no team affiliation in this
example.
             
The Voronoi diagram is calculated for each frame of the video, and the result is
displayed as an animation. The diagram is calculated based on the world positions
of the players, as corresponding to the image/world correspondence entered in the
Pipelines web app.
             
First, build a Pipeline that - at a minimum - contains the following:

1. Video Loader
2. Athlete Detector
3. Athlete Tracker
4. Kinematics Calculator
5. Tracked Bounding Box Writer
             
Ensure that the image/world correspondence has been correctly set on the Video Loader
and that the bounding box writer has the tracked world positions provided as input. 
             
Then, select that Pipeline below, and verify that the Video Loader name is correct.
Finally, upload a video file and start the job. The results will be displayed at the
bottom once the job is complete.
""")

    api_key = st_utils.api_key_input()
    if not api_key:
        st.error('Please enter a valid Pipelines API key in the sidebar')
        return

    picked_pipeline = st_utils.pipeline_picker()

    if picked_pipeline is None:
        return None

    pipeline_id = picked_pipeline['id']

    input_node_id = st_utils.persisted_text_input(key='voronoi.input_node_id',
                                                  label='Video input Node',
                                                  default=DEFAULT_INPUT_NODE_ID,
                                                  help='The name of the video input node.'
                                                  )
    if not input_node_id:
        st.error('Please fill in all the required fields.')
        return

    uploaded_file = st.file_uploader(
        'Select a video file', type=['mp4', 'mkv', 'avi'])

    if not uploaded_file:
        return

    st.markdown('#### Video preview')
    st.video(uploaded_file)

    start_button = st.empty()
    triggered = start_button.button('Start Pipelines job', type='primary')

    if not triggered:
        return

    start_button.empty()

    asyncio.run(on_start_job(uploaded_file.getvalue(),
                             uploaded_file.name,
                             pipeline_id,
                             input_node_id))


if __name__ == '__main__':
    layout_ui()
