import api_utils
import st_utils
import streamlit as st

st.set_page_config(page_title="Pipelines API Streamlit Demo")

st.write("# Pipelines API Examples")

st_utils.api_key_input()

st.markdown("""
    This Streamlit app demonstrates how to use the Pipelines API run computer vision workloads
    on the cloud. To get started, you'll need to enter your Pipelines API key in the sidebar.
    
    If you don't yet have an API key, you can get one by opening a pipeline in the [Pipelines
    web app](https://pipelines.ati.ausport.gov.au/) and hitting the little "profile" icon
    to the top right of the screen. Then click "API Access" to generate and copy your key.
            
    If this menu option is missing, email `AISDevelopers@ausport.gov.au` to request access.
""")
st.info("""
    Your API key is private and should not be shared. You will need to generate a new
    key if you lose it.
""")
st.markdown("""
    Once you've entered your key in the sidebar, you can run the examples to the top left.
    These are purely to demonstrate how to use the API and are not intended to be complete
    solutions in themselves. So, dive into the code, pull out and adapt the parts you need
    for your own projects.
""")
